import { createBrowserRouter } from 'react-router-dom';
import { routes as routesFrontoffice } from './frontoffice';
import { routes as routesBackoffice } from './backoffice';

// merge routes
const routes = [...routesFrontoffice, ...routesBackoffice];


export default createBrowserRouter(routes);