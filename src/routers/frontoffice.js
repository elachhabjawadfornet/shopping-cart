import Layout from '../frontoffice/views/Layout';
import Home from '../frontoffice/views/home/index'

//import Blog from '../frontoffice/views/Blog'

export const routes = [
    {
        path: "/",
        element: <Layout />,
        children: [
            { index: true, element: <Home /> },
            //{ path: '/blog', element: <Blog /> }

        ],
    },
];

