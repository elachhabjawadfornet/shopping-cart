import { useState } from 'react';
import PopularProductHp from './PopularProductHp';


const ProductsHp = () => {

    const [products, setProducts] = useState([
        {
            id: 1,
            image: {
                source: '/images/icons/delivery-van.svg',
                alternative: 'Delivery'
            },
            title: 'Free Shipping',
            price: 500,
            promo: 'dede',
            start: 5,
        }
    ]);

    return (
        <div className="container pb-16">
            <h2 className="text-2xl font-medium text-gray-800 uppercase text-center mb-6">Popular Products</h2>
            <div className="grid grid-cols-2 md:grid-cols-4 gap-6">
                {products.map(product => (
                    <PopularProductHp  {...product}/>
                ))}

            </div>
        </div>
    )
}

export default ProductsHp