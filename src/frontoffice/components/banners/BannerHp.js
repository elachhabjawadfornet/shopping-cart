import React from 'react';
import { Link } from 'react-router-dom';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';

function BannerHp() {


    



    return (

    
        <Swiper
            slidesPerView={1}
        >
            <SwiperSlide className="!h-vh-140 bg-cover bg-no-repeat bg-center py-36" style={{ backgroundImage: "url('/images/banner/banner-1.jpg')" }}>
                <div className="container">
                    <span>SAVE UP TO 40% OFF</span>
                    <h1 className="text-6xl text-primary font-extrabold my-4 capitalize">
                        MEN SUNGLASS
                    </h1>
                    <p className="text-sm text-gray-500">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam <br />
                        accusantium perspiciatis, sapiente
                        magni eos dolorum ex quos dolores odio</p>
                    <div className="mt-12">
                        <Link to="#" className="bg-primary border border-primary text-white px-8 py-3 font-medium 
                     rounded-md hover:bg-transparent hover:text-primary  uppercase mr-4">Shop Now</Link>
                    </div>
                </div>
            </SwiperSlide>

            <SwiperSlide className="!h-vh-140 bg-cover bg-no-repeat bg-center py-36" style={{ backgroundImage: "url('/images/banner/banner-2.jpg')" }}>
                <div className="container">
                    <span>SAVE UP TO 40% OFF</span>
                    <h1 className="text-6xl text-primary font-extrabold my-4 capitalize">
                        MEN SUNGLASS
                    </h1>
                    <p className="text-sm text-gray-500">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam <br />
                        accusantium perspiciatis, sapiente
                        magni eos dolorum ex quos dolores odio</p>
                    <div className="mt-12">
                        <Link to="#" className="bg-primary border border-primary text-white px-8 py-3 font-medium 
                     rounded-md hover:bg-transparent hover:text-primary  uppercase mr-4">Shop Now</Link>
                    </div>
                </div>
            </SwiperSlide>


           

        </Swiper>

    )
}

export default BannerHp