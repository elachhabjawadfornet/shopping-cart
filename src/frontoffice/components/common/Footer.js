import { Link } from 'react-router-dom';
import ScrollToTop from '../ScrollToTop';


const Footer = () => {

  return (

    <>

   
      <ScrollToTop />
      <footer className="bg-gray-800 text-white">
        <div className="container">
          <div className="grid grid-cols-4 gap-x-10 py-24">
            <div>

              <h3 className="font-bold text-[18px] text-white mb-8">About Us</h3>
              <img src="/img/logo.png" alt="" className="block mb-4" />
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.</p>
            </div>

            <div>
              <h3 className="font-bold text-[18px] text-white mb-8">Instragram Feed</h3>
              <ul>
                <li><Link>Home</Link></li>
                <li><Link>Home</Link></li>
                <li><Link>Home</Link></li>
              </ul>
            </div>

            <div>
              <h3 className="font-bold text-[18px] text-white mb-8">Instragram Feed</h3>
              <ul className="grid grid-cols-4 gap-2">
                <li><img src="img/i1.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i2.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i3.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i4.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i5.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i6.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i6.jpg" alt="" className="w-full" /></li>
                <li><img src="img/i6.jpg" alt="" className="w-full" /></li>
              </ul>
            </div>

            <div>
              <h3 className="font-bold text-[18px] text-white mb-8">Newsletter</h3>
              <p className="text-white">Stay update with our latest</p>
              <form>
                <input className="" />
                <button></button>
              </form>
            </div>
          </div>

          <div className="flex justify-between items-center pb-10">
            <p>Copyright ©2023 All rights reserved | This template is made with  by Colorlib</p>
            <div><img src="/images/methods.png" alt="" className="h-5" /></div>
          </div>
        </div>
      </footer>

    </>

  )
}

export default Footer