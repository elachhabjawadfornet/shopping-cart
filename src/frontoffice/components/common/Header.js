
import { NavLink } from "react-router-dom";



const Header = () => {


    return (

        <header className="left-0 top-0 w-full z-50 bg-gradient-to-r from-yellow to-orange font-medium text-[16px] shadow">
            <div className="container py-4 text-white">
                <div className="flex justify-between items-center pb-4  gap-5">
                    <div className="w-3/12"><img src="/img/logo.png" alt="" /></div>
                    <div className="w-6/12 relative">
                        <input type="text" placeholder="Search here" className="block mx-auto w-4/5 py-3 px-6  rounded-full " />
                        <button className="absolute top-1/2 -translate-y-1/2 right-20 h-24 "><i class='bx bx-search  text-gray-800 text-2xl'></i></button>
                    </div>
                    <div className="w-3/12">
                        <ul className="flex justify-between items-center ">
                            <li className="text-center">Available 24/7 at<br /> (090) 123-4567</li>
                            <li><i class='bx bx-heart text-3xl' ></i></li>
                            <li><i class='bx bx-user text-3xl'></i></li>
                            <li><i class='bx bx-shopping-bag text-3xl' ></i></li>

                        </ul>
                    </div>
                </div>

                <div className="flex justify-between items-center ">
                    <ul className="flex justify-center items-center gap-8 ">
                        <li><NavLink to="/">Home</NavLink></li>
                        <li><NavLink to="shop">Shop</NavLink></li>
                        <li><NavLink to="products">Products</NavLink></li>
                        <li><NavLink className="blog">Blog</NavLink></li>
                        <li><NavLink className="contact">Contact</NavLink></li>
                    </ul>

                    <ul className="flex justify-center items-center gap-4">
                        <li className="after:content-['|'] after:ml-2">
                            <NavLink>
                                <i class='bx bx-support mr-2' ></i>
                                Help</NavLink>
                        </li>
                        <li>
                            <NavLink>
                                <i class='bx bx-globe mr-2' ></i>
                                Francais
                            </NavLink>
                        </li>
                    </ul>
                </div>


            </div>
        </header >

    )
}

export default Header