import { NavLink } from "react-router-dom";
import { useState, useEffect } from "react";

const Navbar = () => {


  const [scrolled, setScrolled] = useState(false);

  const handelScroll = () => {
    if (window.scrollY > 100) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", handelScroll)
    return () => {
      window.removeEventListener("scroll", handelScroll)
    }
  }, [])





  return (
    <nav className={`shadow-lg py-2 bg-white text-gray-800`}>
      <div className="container flex justify-between items-center">
      <NavLink><img src="/img/logo.png" alt="logo" /></NavLink>
        <div className="flex basis-auto grow uppercase font-medium">
          <ul className="flex ml-auto ">
            <li className="mr-11"><NavLink to="home">Home</NavLink></li>
            <li className="mr-11"><NavLink to="shop">Shop</NavLink></li>
            <li className="mr-11"><NavLink to="products">Products</NavLink></li>
            <li className="mr-11"><NavLink to="blog">Blog</NavLink></li>
            <li><NavLink to="contact">Contact</NavLink></li>
          </ul>

          <ul className="inline-flex">
            <li className="ml-11"><NavLink><i className="fa-solid fa-cart-shopping"></i></NavLink></li>
            <li className="ml-11"><button><i className="fa-solid fa-magnifying-glass"></i></button></li>
          </ul>
        </div>
      </div>
      
    </nav>
  )
}

export default Navbar