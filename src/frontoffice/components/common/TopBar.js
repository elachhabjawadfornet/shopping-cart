import React from 'react'
import { NavLink } from 'react-router-dom'

function TopBar() {
  return (
      <div className="container flex justify-between py-3 bg-gradient-to-r from-yellow to-orange text-white">
         
          <ul className="flex justify-center items-center gap-4">
              <li className="after:content-['/'] after:ml-2 "><NavLink to="#"><i class="fa-solid fa-phone mr-2"></i>+21265598654 </NavLink></li>
              <li className="after:content-['/'] after:ml-2 "><NavLink to="#"><i class="fa-solid fa-envelope mr-2"></i> lorem@gmail.com </NavLink></li>
              <li><NavLink to="#"><i class="fa-solid fa-location-dot mr-2"></i> lorem ipsom</NavLink></li>
          </ul>

          <ul className="flex justify-center items-center gap-4">
              <li><NavLink to="#"><i class="fa-brands fa-facebook-f"></i></NavLink></li>
              <li><NavLink to="#"><i class="fa-brands fa-twitter"></i></NavLink></li>
              <li><NavLink to="#"><i class="fa-brands fa-instagram"></i></NavLink></li>
              <li><NavLink to="#"><i class="fa-brands fa-youtube"></i></NavLink></li>
              <li><NavLink to="#"><i class="fa-brands fa-youtube"></i></NavLink></li>
              <li><NavLink to="#"><i class="fa-brands fa-pinterest"></i></NavLink></li>
          </ul>
      </div>
  )
}

export default TopBar