import { useState } from 'react';
import SwiperCarousel from '../../../shared/components/SwiperCarousel';
import PostHp from './PostHp';


const PostsHp = () => {

    const [posts, setPosts] = useState([

        {
            id: 1,
            sourceImage: '/images/blog/blog-1.jpg',
            title: 'Goods that are heritage in qualityelit.',
            date: '07 Jan,2023'
        },
        {
            id: 2,
            sourceImage: '/images/blog/blog-2.jpg',
            title: 'Goods that are heritage in qualityelit.',
            date: '07 Jan,2023'
        },
        {
            id: 3,
            sourceImage: '/images/blog/blog-3.jpg',
            title: 'Goods that are heritage in qualityelit.',
            date: '07 Jan,2023'
        },
        {
            id: 4,
            sourceImage: '/images/blog/blog-3.jpg',
            title: 'Goods that are heritage in qualityelit.',
            date: '07 Jan,2023'
        },

    ]);


    return (
        <div className='container py-16'>
            <h2 className="relative text-2xl font-medium text-gray-800 uppercase text-center test">From the blog</h2>

            <SwiperCarousel classes="!pt-16" items={posts} options={
                {
                    spaceBetween: 30,
                    slidesPerView: 3,
                    autoplay: { delay: 3000 },
                }
            }
            >
                <PostHp />
            </SwiperCarousel>

        </div>
    )
}

export default PostsHp