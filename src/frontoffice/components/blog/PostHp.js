import { Link } from 'react-router-dom';

const PostHp = ({ sourceImage, title, date }) => {
    return (
        <div className='h-[250px] text-white !flex flex-col justify-end items-center pb-12' style={{ backgroundImage: "url(" + sourceImage + ")" }}>
            <h3 className='uppercase border-b border-spacing-y-4 mb-4'><Link>{ title }</Link></h3>
            <span>{date}</span>
        </div>
    )
}
export default PostHp