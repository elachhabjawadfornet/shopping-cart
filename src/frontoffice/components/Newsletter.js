import React from 'react'

const  Newsletter = () => {
  return (
      <div className='container py-20'>
          <div className='grid grid-cols-2 gap-6'>
              <div>
                  <h2 className='text-4xl font-bold'>Sign up Newsletter</h2>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
              </div>

              <div>
                  <input type='text' placeholder='Enter your email address' className='w-96 mr-4' />
                  <button className='py-2 px-4 text-white bg-primary'>Subscribe</button>
              </div>
          </div>
      </div>
  )
}

export default Newsletter