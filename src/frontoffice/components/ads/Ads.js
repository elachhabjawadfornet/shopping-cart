import { NavLink } from 'react-router-dom';

const  Ads =() => {
  return (
      <div className="container pb-16">
          <div className='flex gap-6'>
              <div className='w-6/12'>
                  <NavLink>
                      <img src='/images/6.jpg' alt='' className='w-full object-cover' />
                  </NavLink>
              </div>


              <div className='w-6/12'>
                  <NavLink>
                      <img src='/images/7.jpg' alt='' className='w-full object-cover' />
                  </NavLink>
              </div>



          </div>
      </div>
  )
}

export default Ads