import { useState } from 'react';
import SwiperCarousel from '../../../shared/components/SwiperCarousel';
import TestimonialHp from './TestimonialHp';


const TestimonialsHp = () => {

    const [testimonials, seTestimonials] = useState([
        {
            id: 1,
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
            author: 'Jawad EL ACHHAB',
            job: 'Project Manager'
        },
        {
            id: 2,
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
            author: 'Jawad EL ACHHAB',
            job: 'Project Manager'
        },
        {
            id: 3,
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
            author: 'Jawad EL ACHHAB',
            job: 'Project Manager'
        }
    ]);

    const styledSection = {
        backgroundImage: "url('/images/background_2.jpg')",
        backgroundPosition: "center center",
        backgroundAttachment: "fixed",
        backgroundBlendMode: "screen",
        backgroundClip: "border-box",
        backgroundColor: "rgba(0, 0, 0, 0)",
        backgroundSize: "cover",
        backgroundOrigin: "padding-box",
        backgroundRepeat: "no-repeat"
    }

    return (
        <div className='h-[400px] py-16 relative' style={styledSection}>
            <div className='container' >
                <h2 className="text-2xl font-medium text-white uppercase text-center mb-6 pb-16">Testimonials</h2>
                <div className="absolute inset-0 bg-black opacity-50"></div>

                <SwiperCarousel items={testimonials} options={
                    {
                        spaceBetween: 0,
                        slidesPerView: 1,
                        autoplay: { delay: 3000 },
                    }
                } >
                    <TestimonialHp />
                </SwiperCarousel>

            </div>
        </div>
    )
}

export default TestimonialsHp