import React from 'react'

const TestimonialHp = () => {
  return (
      <div className='!flex flex-col justify-center items-center text-center text-white'>
          <p className='mb-8 max-w-5xl'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
          <p className='font-bold text-primary'>Jawad EL ACHHAB</p>
          <p>Project Manager</p>
    </div>
  )
}

export default TestimonialHp