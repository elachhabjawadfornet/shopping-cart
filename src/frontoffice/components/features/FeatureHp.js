
const FeatureHp = ({ image, title, desciption }) => {

  const { source, alternative } = image;
  return (
    <article className="border border-primary rounded-sm px-3 py-6 flex flex-col justify-center items-center gap-5">
      <img src={source} alt={alternative} className="w-12 h-12 object-contain" />
      <div>
        <h4 className="font-medium capitalize text-lg">{title}</h4>
        <p className="text-gray-500 text-sm">{desciption}</p>
      </div>
    </article>
  )
}

export default FeatureHp