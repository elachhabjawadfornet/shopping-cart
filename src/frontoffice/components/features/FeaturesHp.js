import { useState } from 'react';
import FeatureHp from './FeatureHp';

const FeaturesHp = () => {

    const [features, setFeatures] = useState([
        {
            id: 1,
            image: {
                source: '/images/icons/delivery-van.svg',
                alternative: 'Delivery'
            },
            title: 'Free Shipping',
            desciption: 'Order over $200'
        },

        {
            id: 2,
            image: {
                source: '/images/icons/money-back.svg',
                alternative: 'Delivery'
            },
            title: 'Money Rturns',
            desciption: '30 days money returs'
        },
        {
            id: 3,
            image: {
                source: '/images/icons/service-hours.svg',
                alternative: 'Delivery'
            },
            title: '24/7 Support',
            desciption: 'Customer support'
        },
        {
            id: 4,
            image: {
                source: '/images/icons/service-hours.svg',
                alternative: 'Delivery'
            },
            title: '24/7 Support',
            desciption: 'Customer support'
        }
    ]);


    return (
        <div className="container py-16">
            <div className="w-10/12 grid grid-cols-1 md:grid-cols-4 gap-6 mx-auto justify-center">

                {features.map(feature => (
                    <FeatureHp {...feature} />
                ))}

            </div>
        </div>
    )
}

export default FeaturesHp