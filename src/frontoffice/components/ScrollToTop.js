import { useState, useEffect } from 'react';

const ScrollToTop = () => {

    const [showTopBtn, setShowTopBtn] = useState(false);

    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };

    const handelScroll = () => {
        if (window.scrollY > 400) {
            setShowTopBtn(true);
        } else {
            setShowTopBtn(false);
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', handelScroll);

        return () => {
            window.removeEventListener('scroll', handelScroll);
        };

    }, []);

    return (
        <>
            {showTopBtn && <span onClick={goToTop} className="cursor-pointer fixed right-5 bottom-5 w-10 h-10 bg-gradient-to-r from-yellow to-orange text-white rounded-lg flex justify-center items-center">
                <i class='bx bx-up-arrow-alt' ></i>
            </span>}</>
    )
}

export default ScrollToTop