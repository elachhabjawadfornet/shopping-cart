import { Link, } from 'react-router-dom';
import SwiperCarousel from '../../../shared/components/SwiperCarousel';
import Banner from './components/banner';
import Features from './components/Features';
import Products from './components/products';
import Brands from './components/brands';
import LatestPosts from './components/LatestPosts';
import Categories from './components/Categories';


const Home = () => {
    return (
        <>

            <Banner />

            <Features />

            <Categories />

            <Products />

            <section className="flex items-center">
                <div className="w-6/12  relative py-[275px]">
                    <img src="/img/exclusive.jpg" alt="" className="absolute left-0 top-0 w-full h-full -z-10" />
                    <div className="container text-center px-[135px]">
                        <h2 className="text-white text-[36px]">
                            Exclusive Hot Deal Ends Soon!
                        </h2>
                        <p className="text-white my-10">Who are in extremely love with eco friendly system.</p>

                        <div className="bg-white py-[18px] rounded-md grid grid-cols-4 mb-10">
                            <div className="text-gray-800 border-r border-black/10">
                                <span className="block text-[36px] py-3 font-medium">29</span>
                                <span className="block">Days</span>
                            </div>
                            <div className="text-gray-800 border-r border-black/10">
                                <span className="block text-[36px] py-3 font-medium">29</span>
                                <span className="block">Days</span>
                            </div>
                            <div className="text-gray-800 border-r border-black/10">
                                <span className="block text-[36px] py-3 font-medium">29</span>
                                <span className="block">Days</span>
                            </div>
                            <div className="text-gray-800 border-r border-black/10">
                                <span className="block text-[36px] py-3 font-medium">29</span>
                                <span className="block">Days</span>
                            </div>
                        </div>

                        <Link className="inline-block py-3 px-7 bg-gradient-to-r from-yellow to-orange text-white rounded-lg uppercase">Shop now</Link>

                    </div>
                </div>
                <div className="w-6/12">

                    <div className="container">
                        <SwiperCarousel items={[{}, {}, {}, {}, {}, {}]} options={
                            {
                                spaceBetween: 0,
                                slidesPerView: 1,
                                //autoplay: { delay: 3000 },
                            }}>
                            <article className="text-center ">
                                <div className="">
                                    <img src="/img/product/e-p1.png" alt="" />
                                </div>
                                <div className="">
                                    <span className="inline-block text-[14px] mr-5">$150.00</span>
                                    <span>$210.00</span>
                                </div>
                                <h3 className="text-[24px] uppercase">Lorem ipsum dolor sit amet, consectetur adipisicing elit,</h3>
                            </article>
                        </SwiperCarousel>
                    </div>

                </div>
            </section>

            <Brands />

            <LatestPosts />

        </>
    )
}

export default Home