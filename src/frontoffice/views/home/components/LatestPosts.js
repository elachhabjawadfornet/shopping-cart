import { useState } from 'react'

const LatestPosts = () => {
    const [posts, setPosts] = useState([
        {
            id: 1,
            title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            image: {
                source: '/img/blog/latest-post/l-post-1.jpg',
                alternative: 'Post 1'
            }
        },
        {
            id: 2,
            title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            image: {
                source: '/img/blog/latest-post/l-post-2.jpg',
                alternative: 'Post 2'
            }
        },
        {
            id: 3,
            title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,',
            image: {
                source: '/img/blog/latest-post/l-post-3.jpg',
                alternative: 'Post 2'
            }
        },
    ]);


    return (
        <section className="container py-24">
            <div className="mb-[50px] text-center">
                <h1 className="text-[36px]">Our Blog</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
            </div>

            <div className="grid grid-cols-3 gap-8">
                {posts.map(({ id, title, description, image: { source, alternative } }) =>
                    <article key={id}>
                        <div className="mb-[15px]"><img src={source} alt={alternative} className="w-full h-full bg-cover" /></div>
                        <h3 className="text-[16px] mb-3">{title}</h3>
                        <p>{description}</p>
                    </article>)}
            </div>
        </section>
    )
}

export default LatestPosts