import { useState } from "react"

const Features = () => {

    const [features, setFeatures] = useState([
        {
            id: 1,
            image: {
                source: '/img/features/f-icon1.png',
                alternative: 'Free Delivery'
            },
            title: 'Free Delivery',
            description: 'Lorem ipsum dolor sit amet'
        },
        {
            id: 2,
            image: {
                source: '/img/features/f-icon2.png',
                alternative: 'Free Delivery'
            },
            title: 'Free Delivery',
            description: 'Lorem ipsum dolor sit amet'
        },
        {
            id: 3,
            image: {
                source: '/img/features/f-icon3.png',
                alternative: 'Free Delivery'
            },
            title: 'Free Delivery',
            description: 'Lorem ipsum dolor sit amet'
        },
        {
            id: 4,
            image: {
                source: '/img/features/f-icon4.png',
                alternative: 'Free Delivery'
            },
            title: 'Free Delivery',
            description: 'Lorem ipsum dolor sit amet'
        }
    ])



    return (
        <section className="container py-24">
            <div className="bg-white drop-shadow-xl grid grid-cols-4 py-10 text-center">

                {features.map(({ id, image: { source, alternative }, title, description }) => (
                    <article key={id} className="flex flex-col justify-center items-center border-r border-black/10">
                        <div className="mb-5">
                            <img src={source} alt={alternative} />
                        </div>

                        <h3 className="font-base">{title}</h3>
                        <p>{description}</p>
                    </article>
                ))}

            </div>
        </section>
    )
}

export default Features