import React from 'react';
import { Link } from 'react-router-dom';

const Post = ({ id, title, description, image }) => {

    const { source, alternative } = image;
    
    return (
        <article className="flex justify-center items-center gap-x-8">
            <div className="w-5/12">
                <h1 className="text-5xl font-bold text-black" dangerouslySetInnerHTML={{ '__html': title }}></h1>
                <p className="my-8">{description}</p>


                <Link className="inline-block py-3 px-7 bg-gradient-to-r from-yellow to-orange text-white rounded-lg uppercase">Shop now</Link>
            </div>
            <div className="w-7/12">

                <img className="w-full h-full object-cover" src={source} alt={alternative} />

            </div>
        </article>
    )
}

export default Post