import { useState } from 'react';
import SwiperCarousel from '../../../../../shared/components/SwiperCarousel';
import Post from './Post';


const Banner = () => {

    const [posts, setPosts] = useState(
        [
            {
                id: 1,
                title: 'Nike New <br />Collection!',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation.',
                image: {
                    source: '/img/banner/banner-img.png',
                    alternative: 'Product 1'
                }
            },
            {
                id: 2,
                title: 'Nike New <br />Collection!',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation.',
                image: {
                    source: '/img/banner/banner-img.png',
                    alternative: 'Product 2'
                }
            }
        ]
    );

    

    return (
        <section className="h-banner relative bg-cover" style={{ backgroundImage: "url('/img/banner/banner-bg.jpg')" }}>
            <div className="container h-full flex justify-center items-center">
                <SwiperCarousel items={posts} options={
                    {
                        spaceBetween: 0,
                        slidesPerView: 1,
                        autoplay: { delay: 3000 },
                    }}>
                    <Post />
                </SwiperCarousel>
            </div>
        </section>
    )
}

export default Banner