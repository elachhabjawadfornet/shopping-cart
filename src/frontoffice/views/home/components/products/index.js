import SwiperCarousel from '../../../../../shared/components/SwiperCarousel';
import { NavLink } from 'react-router-dom';

const Products =()=> {
  return (
      <section className="container py-24">
          <div className="mb-[50px] text-center">
              <h1 className="text-[36px]">Latest products</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, </p>
          </div>

          <SwiperCarousel items={[{}, {}, {}, {}, {}, {}]} options={
              {

                  spaceBetween: 30,
                  slidesPerView: 4,
                  // autoplay: { delay: 3000 },

              }}>
              <article className="mb-[50px]">
                  <div className="w-full mb-[20px]">
                      <img src="/img/product/p1.jpg" alt="" className="w-full h-full bg-cover" />
                  </div>
                  <div>
                      <h3 className="text-[16px] uppercase mb-3">addidas New Hammer sole
                          for Sports person</h3>
                      <div>
                          <h6 className="inline-block text-[14px] mr-[15px]">500$</h6>
                          <h6 className="inline-block text-[14px] mr-[15px] text-[#cccccc] line-through">500$</h6>
                      </div>

                      <ul className="flex">
                          <li>
                              <NavLink>
                                  <i class="fa-solid fa-bag-shopping"></i>
                                  <p className="hidden">Add to bag</p>
                              </NavLink>
                          </li>
                          <li>
                              <NavLink>
                                  <i class="fa-solid fa-heart"></i>
                                  <p className="hidden">Add to bag</p>
                              </NavLink>


                          </li>
                          <li>
                              <NavLink>
                                  <i class="fa-solid fa-arrows-rotate"></i>
                                  <p className="hidden">Add to bag</p>
                              </NavLink>

                          </li>
                          <li>
                              <NavLink>
                                  <i class="fa-solid fa-circle-info"></i>
                                  <p className="hidden">Add to bag</p>
                              </NavLink>

                          </li>

                      </ul>
                  </div>

              </article>
          </SwiperCarousel>



      </section>
  )
}

export default Products