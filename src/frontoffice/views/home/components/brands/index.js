import SwiperCarousel from "../../../../../shared/components/SwiperCarousel";
import { NavLink } from "react-router-dom";
import { useState } from "react";
import { SwiperSlide } from 'swiper/react';

const Brands = (props) => {

    const [brands, setBrands] = useState([

        {
            link: 'test 1',
            image: {
                source: '/img/brand/1.png',
                alternative: 'Product 1'
            }
        },

        {
            link: 'test 2',
            image: {
                source: '/img/brand/1.png',
                alternative: 'Product 1'
            }
        }

    ]);

    console.log(props);
    return (
        <section className="container py-10">
            <SwiperCarousel items={brands} options={
                {
                    spaceBetween: 30,
                    slidesPerView: 5,
                    
                }}>
                <SwiperSlide></SwiperSlide>
                <article className="flex justify-center items-center gap-x-8 ">
                    <NavLink to={JSON.stringify(props)}><img src={"props.image.source"} alt={"alternativee"} className="filter grayscale-0" /></NavLink>
                </article>
            </SwiperCarousel>
        </section>
    )
}

export default Brands