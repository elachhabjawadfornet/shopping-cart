import { useState } from "react";


const Categories = () => {

    const [categories, setCategories] = useState([
        {
            id: 1,
            image: {
                source: '/img/category/c1.jpg',
                alternative: 'c1'
            }
        },
        {
            id: 2,
            image: {
                source: '/img/category/c2.jpg',
                alternative: 'c2'
            }
        },
        {
            id: 3,
            image: {
                source: '/img/category/c3.jpg',
                alternative: 'c3'
            }
        },
        {
            id: 4,
            image: {
                source: '/img/category/c4.jpg',
                alternative: 'c4'
            }
        },

        {
            id: 5,
            image: {
                source: '/img/category/c5.jpg',
                alternative: 'c5'
            }
        }
    ]);

    const { source: lastCatSource, alternative: lastCatAlternative } = categories[categories.length - 1].image;

    return (
        <section className="container">
            <div className="flex  gap-4">
                <div className="w-8/12">
                    <div className="grid grid-cols-6 gap-4">
                        {categories.slice(0, 4).map(({ id, image: { source, alternative } }, index) => (
                            <div key={id} className={`col-span-${index === 0 || index === 3 ? '4' : '2'}`}>
                                <img src={source} alt={alternative} className="w-full h-full bg-cover" />
                            </div>
                        ))}
                    </div>
                </div>
                <div className="w-4/12">
                    <div>
                        <img src={lastCatSource} alt={lastCatAlternative} className="w-full h-full bg-cover" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Categories