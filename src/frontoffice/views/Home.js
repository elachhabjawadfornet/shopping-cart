import { Swiper, SwiperSlide } from 'swiper/react';
import { NavLink } from 'react-router-dom';
import 'swiper/css';

import BannerHp from '../components/banners/BannerHp';
import FeaturesHp from '../components/features/FeaturesHp';
import TestimonialsHp from '../components/testimonials/TestimonialsHp';
import PostsHp from '../components/blog/PostsHp';
import Newsletter from '../components/Newsletter';
import Ads from '../components/ads/Ads';
import PopularProductsHp from '../components/products/PopularProductsHp';


const Home = () => {

    return (
        <>

            {/* banner hp */}
            <BannerHp />
            {/* ./banner hp */}


            {/* features */}
            <FeaturesHp />
            {/* ./features */}

            {/* new arrivals */}
            <div className='container pb-16 flex gap-10'>
                <div className='w-3/12'>
                    <h2>New Arrivals</h2>
                    <p>Donec nec justo eget felis facilisis fermentum.</p>
                    <ul className='p-5  text-white' style={{ backgroundImage: "url('/images/bg_tab_cates.jpg')" }}>
                        <li className='cursor-pointer block capitalize border bg-primary p-3  mb-3'>Layout<span className='float-right'>10</span></li>
                        <li className='cursor-pointer block capitalize border p-3  mb-3'>Layout<span className='float-right'>10</span></li>
                        <li className='cursor-pointer block capitalize border p-3 mb-3'>Layout<span className='float-right'>10</span></li>
                        <li className='cursor-pointer block capitalize border p-3  mb-3'>Layout<span className='float-right'>10</span></li>
                        <li className='cursor-pointer block capitalize border p-3  mb-3'>Layout<span className='float-right'>10</span></li>
                    </ul>
                </div>
                <div className='w-9/12'>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={20}
                    >
                        <SwiperSlide className='bg-white shadow rounded overflow-hidden group'>
                            <div className="relative">
                                <img src="/images/products/product1.jpg" alt="product 1" className="w-full" />
                                <div className="absolute inset-0 bg-black bg-opacity-40 flex items-center 
                    justify-center gap-2 opacity-0 group-hover:opacity-100 transition">
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="view product">
                                        <i className="fa-solid fa-magnifying-glass"></i>
                                    </NavLink>
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="add to wishlist">
                                        <i className="fa-solid fa-heart"></i>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="pt-4 pb-3 px-4">
                                <NavLink to="#">
                                    <h4 className="uppercase font-medium text-xl mb-2 text-gray-800 hover:text-primary transition">Guyer
                                        Chair</h4>
                                </NavLink>
                                <div className="flex items-baseline mb-1 space-x-2">
                                    <p className="text-xl text-primary font-semibold">$45.00</p>
                                    <p className="text-sm text-gray-400 line-through">$55.90</p>
                                </div>
                                <div className="flex items-center">
                                    <div className="flex gap-1 text-sm text-yellow-400">
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                    </div>
                                    <div className="text-xs text-gray-500 ml-3">(150)</div>
                                </div>
                            </div>
                            <NavLink to="#"
                                className="block w-full py-1 text-center text-white bg-primary border border-primary rounded-b hover:bg-transparent hover:text-primary transition">Add
                                to cart</NavLink>
                        </SwiperSlide>

                        <SwiperSlide className='bg-white shadow rounded overflow-hidden group'>
                            <div className="relative">
                                <img src="/images/products/product1.jpg" alt="product 1" className="w-full" />
                                <div className="absolute inset-0 bg-black bg-opacity-40 flex items-center 
                    justify-center gap-2 opacity-0 group-hover:opacity-100 transition">
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="view product">
                                        <i className="fa-solid fa-magnifying-glass"></i>
                                    </NavLink>
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="add to wishlist">
                                        <i className="fa-solid fa-heart"></i>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="pt-4 pb-3 px-4">
                                <NavLink to="#">
                                    <h4 className="uppercase font-medium text-xl mb-2 text-gray-800 hover:text-primary transition">Guyer
                                        Chair</h4>
                                </NavLink>
                                <div className="flex items-baseline mb-1 space-x-2">
                                    <p className="text-xl text-primary font-semibold">$45.00</p>
                                    <p className="text-sm text-gray-400 line-through">$55.90</p>
                                </div>
                                <div className="flex items-center">
                                    <div className="flex gap-1 text-sm text-yellow-400">
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                    </div>
                                    <div className="text-xs text-gray-500 ml-3">(150)</div>
                                </div>
                            </div>
                            <NavLink to="#"
                                className="block w-full py-1 text-center text-white bg-primary border border-primary rounded-b hover:bg-transparent hover:text-primary transition">Add
                                to cart</NavLink>
                        </SwiperSlide>

                        <SwiperSlide className='bg-white shadow rounded overflow-hidden group'>
                            <div className="relative">
                                <img src="/images/products/product1.jpg" alt="product 1" className="w-full" />
                                <div className="absolute inset-0 bg-black bg-opacity-40 flex items-center 
                    justify-center gap-2 opacity-0 group-hover:opacity-100 transition">
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="view product">
                                        <i className="fa-solid fa-magnifying-glass"></i>
                                    </NavLink>
                                    <NavLink to="#"
                                        className="text-white text-lg w-9 h-8 rounded-full bg-primary flex items-center justify-center hover:bg-gray-800 transition"
                                        title="add to wishlist">
                                        <i className="fa-solid fa-heart"></i>
                                    </NavLink>
                                </div>
                            </div>
                            <div className="pt-4 pb-3 px-4">
                                <NavLink to="#">
                                    <h4 className="uppercase font-medium text-xl mb-2 text-gray-800 hover:text-primary transition">Guyer
                                        Chair</h4>
                                </NavLink>
                                <div className="flex items-baseline mb-1 space-x-2">
                                    <p className="text-xl text-primary font-semibold">$45.00</p>
                                    <p className="text-sm text-gray-400 line-through">$55.90</p>
                                </div>
                                <div className="flex items-center">
                                    <div className="flex gap-1 text-sm text-yellow-400">
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                        <span><i className="fa-solid fa-star"></i></span>
                                    </div>
                                    <div className="text-xs text-gray-500 ml-3">(150)</div>
                                </div>
                            </div>
                            <NavLink to="#"
                                className="block w-full py-1 text-center text-white bg-primary border border-primary rounded-b hover:bg-transparent hover:text-primary transition">Add
                                to cart</NavLink>
                        </SwiperSlide>

                    </Swiper>
                </div>
            </div>
            {/* ./new arrivals */}


            {/* ads */}
            <Ads />
            {/* ./ads */}


            {/* popular products */}
            <PopularProductsHp />
            {/* ./popular products */}


            {/* ads */}
            <Ads />
            {/* ./ads */}


            {/* posts hp */}
            <PostsHp />
            {/* ./posts hp */}


            {/* testimonials hp */}
            <TestimonialsHp />
            {/* ./testimonials hp */}


            {/* Newsletter */}
            <Newsletter />
            {/* newsletter */}

        </>
    )
}

export default Home