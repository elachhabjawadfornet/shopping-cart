import { Navigation, Pagination, Autoplay } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import React from 'react';

const SwiperCarousel = ({options, children }) => {

    
    const swiperOptions = {
        ...options,
        navigation: { nextEl: ".arrow-left", prevEl: ".arrow-right" },
        watchSlidesVisibility: true
    }

    
    return (

        <Swiper
            modules={[Navigation, Pagination, Autoplay]}
            {...swiperOptions}
        >
            {children}
        </Swiper>
    ); 
};

export default SwiperCarousel;
