import { Navigation, Pagination, Autoplay } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import React from 'react';

const SwiperCarousel = ({ classes, items, options, children }) => {

    const cloneItem = items.map((el, index) => (
        <SwiperSlide key={index}>
            {React.cloneElement(children, { ...el })}
        </SwiperSlide>
    ));

    const swiperOptions = {
        ...options,
        navigation: { nextEl: ".arrow-left", prevEl: ".arrow-right" },
        watchSlidesVisibility: true
    }


    return (

        <Swiper
            modules={[Navigation, Pagination, Autoplay]}
            {...swiperOptions}
            className={classes}
        >
            {cloneItem}

            <div className='hidden arrow-swiper absolute top-3  -translate-x-2/4 left-1/2 flex gap-5'>
                <button className= 'arrow-left'><i class="fa fa-long-arrow-left text-xl text-[#999]"></i></button>
                <button className='arrow-right'><i class="fa-solid fa-arrow-right text-xl text-[#999]"></i></button>
           </div>
        </Swiper>
    ); 
};

export default SwiperCarousel;
