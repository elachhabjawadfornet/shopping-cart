/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screen: {
      sm: "576px",
      md: "768px",
      lg: "992px",
      xl: "1200px",
    },
    container: {
      center: true,
      padding: "6rem",
    },
    colors: {
      white:"#ffffff",
      black: "#000000",
      red: "#c5322d",
      yellow: "#ffba00",
      orange: "#ff6c00",
      gray: {
        500: "#fcfcfc",
        600: "#777777",
        700: "#828bb3",
        800: "#222222"
      }
    },
    extend: {
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
        roboto: ["Roboto", "sans-serif"],
      },
      fontSize: {
        sm: '0.8rem',
        base: '1rem', // h6
        xl: '1.25rem',
        '2xl': '1.563rem',
        '3xl': '1.953rem',
        '4xl': '2.25rem', //h2
        '5xl': '3.75rem', //h1
      },
      height:{
        'banner':'calc(100vh - 122px)'
      }
    },
  }
}